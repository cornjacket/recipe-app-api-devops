variable "prefix" {
  default = "raad" # recipe-app-api-devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "taylor.david.ray@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion" # matches key name of key-pair made on aws
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "923799911166.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "923799911166.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
  // no default since defined in gitlabs ci/cd
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "gunsandpancakes.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}